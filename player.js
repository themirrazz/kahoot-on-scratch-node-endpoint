const Kahoot=require('./kahootjs-working');

var game=new Kahoot;
var question=null;
console.log("WHAT THE FRICK, MAN?!")
game.on('Joined', function () {console.log('e'); process.send({type:'connection'}) });
game.on('QuizStart', function () { process.send({type:'quiz_start'}) });
game.on('QuestionStart', function (quiz) {question=quiz; process.send({type:'new_question', value: {
	type: quiz.gameBlockType,
	answers: quiz.quizQuestionAnswers
}}) });
game.on('QuestionEnd', function (q) {if(!question){return};question=null; process.send({type:'question_over',value:q.isCorrect?"correct":"incorrect"}) });
game.on('QuestionReady', function () { process.send({type:'before_question'}) });
game.on('QuizEnd', function () { process.send({type:'quiz_end'}) });
game.on('Podium', function (e) { process.send({type:'gets_medal', value: e.podiumMedalType}) });

game.on("TwoFactorReset", function (e) { process.send({type:'verificationRequired_2fa'}) });
game.on("TwoFactorWrong", function (e) { process.send({type:'2fa_failed'}) });
game.on("TwoFactorCorrect", function (e) { process.send({type:'2fa_accept'}) });
game.on("TimeOver", function (e) {question=null;process.send({type:'question_over',value:"time"}) })

process.on('message', message => {
	console.log("monchie comin at ya!")
	if(message.type==='connect') {
		game.join(message.value.pin,message.value.username).catch(error => {
		console.log(error);
			process.send({
				type: 'system_error',
				value: error
			});
		})
	} else if(message.type==="complete2fa") {
		game.answerTwoFactorAuth(message.value);
	} else if(message.type==="answer") {
		try {
			question.answer(message.value);
		} catch (error) {
			null
		}
	}
})

