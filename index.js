const Kahoot = require('./kahootjs-working');
const Scratch = require("scratch3-api");


const os=require('os');
const chproc=require('child_process');

console.log(chproc);
var kahootSessions = {};


function createKahootInstance(sessionId,nickname,gamePin) {
	var events=[];
  var instance = {
  	process: null,
  	sendAnswer: function(ans) {
  		instance.process.send({type:'answer',value:ans});
  	},
  	on: function (name, callback) {
  		events.push({name,callback})
  	},
  	emit:function(name,event) {
  		events.forEach(handler => {
  			if(handler.name===name) {
  				try {
  					handler.callback(event)
  				} catch (error) {
  					console.warn("Error in callback:",error);
  				}
  			}
  		})
  	}
  };
  var gamePath=(__dirname+(os.platform()==="win32"?"\\":"/")+"player.js");
  var proc=chproc.fork(gamePath,[sessionId],{
  	stdio: 'inherit'
  });
  proc.onerror=function(e){console.error(e)}
  proc.on('message', function(message) {
  console.log(message)
  	if(message.type==="duplicate_nickname") {
  		instance.emit("multiaccount");
  	} else if(message.type==="quiz_start") {
  		instance.emit("start");
  	} else if(message.type==="new_question") {
  		instance.emit("questionbegin", {
  			questionType: message.value.type,
  			numberOfAnswers: message.value.answers.length
  		});
  	} else if(message.type==="question_over") {
  		instance.emit("questionend", {
  			status: message.value
  		})
  	} else if(message.type==="host_kicks") {
  		instance.emit("clientkicked");
  	} else if(message.type==="wrong_pin") {
  		instance.emit("invalidpin");
  	} else if(message.type==="system_error") {
  		instance.emit("error",message.value);
  	} else if(message.type==="quiz_end") {
  		instance.emit("end");
  	} else if(message.type==="connection") {
  		instance.emit("connect");
  	}
  });
  kahootSessions[sessionId]=instance;
  instance.process=proc;
  setTimeout(function(){
  	proc.send({type: "connect", value:{username: "@"+nickname+" (via Scratch)",pin:gamePin}})
  },1000);
  return instance;
};



var callTypeMap = {
  "00": "USER_CREATE_SESSION",
  "01": "USER_SELECT_ANSWER",
  "02": "SERVER_ERROR",
  "03": "SERVER_GAME_CONNECTED",
  "04": "SERVER_QUIZ_START",
  "05": "SERVER_QUESTION_START",
  "06": "SERVER_QUESTION_END",
  "07": "SERVER_QUIZ_END",
  "08": "SERVER_KICKED"
}

async function main() {
  var session = await Scratch.UserSession.create(
    "crater24",
    'coolDoggie80'
  );
  await session.prompt()
  var cloud = await session.cloudSession("722364906");
  cloud.on('set', function (rawName,value) {
  	var name=rawName.slice(2)
  	if(name!="UserChannel") {
  		return
  	}
  	if(value==0) {
  		return
  	}
  	var data=String(value).slice(1); // cut of the extra bit
  	cloud.set(rawName,0); // reset the user-channel for other users
  	var sessionId=data.slice(0,11);
  	var callType=data.slice(11,13);
  	var paramaters=data.slice(13);
  	console.log('sid:',sessionId)
  	console.log('call:',callType);
  	console.log(callType=="00");
  	console.log('par:',paramaters);
  	if(callType=="00") {
  		var instance=createKahootInstance(sessionId,cloud.stringify(paramaters.split("00")[0]+"00"),paramaters.split("00")[1]);
  		instance.on("error", function (error) {
  			console.log(error);
  			cloud.set(cloud.name("ServerChannel"),`1${sessionId}02`)
  		});
  		instance.on("questionbegin", function (question) {
  			cloud.set(cloud.name("ServerChannel"),`1${sessionId}05${question.numberOfAnswers}`)
  		});
  		instance.on("questionend", function (question) {
  			cloud.set(cloud.name("ServerChannel"),Number(`1${sessionId}06${
  			(function () {
  			if(question.status=='correct'){return 0} else if(question.status=='incorrect') {return 1} else {return 2}
  			})()
  			}`))
  		});
  		instance.on("start", function (question) {
  			cloud.set(cloud.name("ServerChannel"),`1${sessionId}04`)
  		});
  		instance.on("end", function (question) {
  			cloud.set(cloud.name("ServerChannel"),`1${sessionId}07`)
  		});
  		instance.on("connect", function (question) {
  			cloud.set(cloud.name("ServerChannel"),`1${sessionId}03`)
  		});
  	}
  })
}

main();
