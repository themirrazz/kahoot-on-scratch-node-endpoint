"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const prompt_1 = __importDefault(require("prompt"));
const util_1 = __importDefault(require("util"));
const cloudsession_js_1 = __importDefault(require("./cloudsession.js"));
const projects_js_1 = __importDefault(require("./projects.js"));
const request_js_1 = require("./request.js");
const parse = function (cookie) {
    let c = {};
    let e = cookie.split(";");
    for (let p of e) {
        if (p.indexOf("=") === -1) {
            continue;
        }
        let s = p.split("=");
        c[s[0].trim()] = s[1].trim();
    }
    return c;
};
/**
 * UserSession API for managing user data.
 * @param {boolean} loaded - whether or not a username and password have been loaded into the session.
 * @param {boolean} valid - the login was sucessful and accepted by the scratch servers.
 */
class UserSession {
    /**
     * Create a blank UserSession
     */
    constructor() {
        this.sessionId = "";
        this.username = "";
        this.password = "";
        this.id = 0;
        this.token = "";
        this.loaded = false;
        this.valid = false;
    }
    /**
     * Create a new UserSession with the given username and password.
     * @async
     * @param  {string} [username] - The username to log in with. If missing user will be prompted.
     * @param  {string} [password] - The password to log in with. If missing user will be prompted.
     * @returns {UserSession} - A loaded UserSession.
     */
    static create(username, password) {
        return __awaiter(this, void 0, void 0, function* () {
            let s = new this();
            yield s.load(username, password);
            return s;
        });
    }
    /**
     * Acess Projects API.
     * @returns {Projects} - Project API
     */
    get projects() {
        return new projects_js_1.default(this);
    }
    /**
     * Load a blank UserSession with the given username and password.
     * @async
     * @param  {string} [username] - The username to log in with. If missing user will be prompted.
     * @param  {string} [password] - The password to log in with. If missing user will be prompted.
     */
    load(username = "", password = "") {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.loaded)
                return;
            let un = username, pw = password;
            if (!username) {
                prompt_1.default.start();
                let r = yield new Promise(function (resolve, reject) {
                    prompt_1.default.get([
                        {
                            name: "Username",
                            required: true
                        }
                    ], function (e, r) {
                        if (e)
                            reject(e);
                        else
                            resolve(r);
                    });
                });
                un = r.Username;
            }
            this.username = un;
            if (!password) {
                prompt_1.default.start();
                let r = yield new Promise(function (resolve, reject) {
                    prompt_1.default.get([
                        {
                            name: "Password",
                            required: true
                        }
                    ], function (e, r) {
                        if (e)
                            reject(e);
                        else
                            resolve(r);
                    });
                });
                pw = r.Password;
            }
            this.password = pw;
            let [err, body, res] = yield (0, request_js_1.request)({
                path: "/login/",
                method: "POST",
                body: JSON.stringify({
                    username: this.username,
                    password: this.password
                }),
                headers: {
                    "X-Requested-With": "XMLHttpRequest"
                }
            });
            if (err)
                throw new Error(err);
            try {
              console.log(body);
                let u = JSON.parse(body)[0];
                if (u.msg)
                    throw new Error(u.msg);
                this.id = u.id;
                this.sessionId = parse(res.headers["set-cookie"][0]).scratchsessionsid;
                this.loaded = true;
                this.token = u.token;
                return;
            }
            catch (e) {
                if (e instanceof SyntaxError)
                    throw new Error("Scratch servers are down. Try again later.");
                throw new Error(e);
            }
        });
    }
    /**
     * Prompt the user for a username amnd password to load the UserSession with
     * @async
     */
    prompt() {
        return __awaiter(this, void 0, void 0, function* () {
            yield new Promise(function (resolve) {
                return setTimeout(resolve, 0);
            }); //Allow deprecation warning to show before prompt
            return yield this.load();
        });
    }
    /**
     * Verify the loaded UserSession
     * @returns {boolean} Whether the session is valid or not
     */
    verify() {
        return __awaiter(this, void 0, void 0, function* () {
            let [e, body, res] = yield (0, request_js_1.request)({});
            this.valid = !e && res.statusCode === 200;
            return this.valid;
        });
    }
    /**
     * Add a comment
     * @async
     * @param {Object} o - Options
     * @param {string|number} [o.project] - Project to comment on. Overrides user and studio properties.
     * @param {string} [o.user] - User to comment on. Overrides studio property.
     * @param {string|number} [o.studio] - Studio to comment on.
     * @param {string|number} [o.parent] - Comment to reply to.
     * @param {string} [o.replyto] - The user id to address (@username ...).
     * @param {string} [o.content=""] - The text of the comment to post.
     */
    comment(o) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.valid) {
                yield this.verify();
            }
            let t, id;
            if (o.project) {
                t = "project";
                id = o.project;
            }
            else if (o.user) {
                t = "user";
                id = o.user;
            }
            else if (o.studio) {
                t = "gallery";
                id = o.studio;
            }
            yield (0, request_js_1.request)({
                hostname: "scratch.mit.edu",
                headers: {
                    referer: `https://scratch.mit.edu/users/${this.username}`,
                    "X-Requested-With": "XMLHttpRequest",
                    "x-csrftoken": "a",
                    Cookie: `scratchcsrftoken=a;scratchlanguage=en;scratchsessionsid=${this.sessionId};`
                },
                path: "/site-api/comments/" + t + "/" + id + "/add/",
                method: "POST",
                body: JSON.stringify({
                    content: o.content,
                    parent_id: o.parent || "",
                    commentee_id: o.replyto || ""
                }),
                sessionId: this.sessionId
            });
        });
    }
    /**
     * Create a new CloudSession with the current UserSession.
     * @param {string|number} proj - ID of the project to connect to.
     * @returns {CloudSession} A loaded CloudSession.
     */
    cloudSession(proj, turbowarp = false) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield cloudsession_js_1.default.create(this, proj, turbowarp);
        });
    }
}
UserSession.prototype.prompt = util_1.default.deprecate(UserSession.prototype.prompt, "<UserSession>.prompt is deprecated. Use <UserSession>.load without parameters instead.");
exports.default = UserSession;
